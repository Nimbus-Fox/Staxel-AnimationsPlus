﻿using System;

namespace NimbusFox.AnimationsPlus.Classes.Exceptions {
    public class TileAnimationsConfigException : Exception {
        public TileAnimationsConfigException() { }

        public TileAnimationsConfigException(string message) : base(message) { }

        public TileAnimationsConfigException(string message, Exception innerException) : base(message, innerException) { }
    }
}
