﻿using System;
using System.Linq;
using NimbusFox.AnimationsPlus.Classes.Exceptions;
using Plukit.Base;

namespace NimbusFox.AnimationsPlus.Classes {
    public class TileAnimation {

        public enum TileAnimationType {
            Hover,
            Shake,
            Rotate,
            GrowShrink,

            None
        }

        // Hover Properties
        public double HoverMax { get; } = 0.125;

        public double HoverMin { get; } = 0.025;

        public double HoverStep { get; } = 0.00015;
        // Hover Properties

        // Shake Properties

        // Shake Properties

        // Rotate Properties
        public double XRotate { get; }
        public double YRotate { get; }
        public double ZRotate { get; }
        // Rotate Properties

        // GrowShrink Properties

        // GrowShrink Properties

        // Movement Properties

        // Movement Properties

        public TileAnimationType Type { get; } = TileAnimationType.None;

        public TileAnimation(Blob config) {
            if (!config.Contains("type")) {
                throw new TileAnimationsConfigException("Each animation blob must have a type");
            }

            var type = config.GetString("type");

            foreach (var ty in Enum.GetNames(typeof(TileAnimationType))) {
                if (string.Equals(type, ty, StringComparison.CurrentCultureIgnoreCase)) {
                    Type = (TileAnimationType) Enum.Parse(typeof(TileAnimationType), ty);
                }
            }

            if (Type == TileAnimationType.None) {
                throw new TileAnimationsConfigException(
                    "The animation type must be valid and must not be none. Valid types are: " +
                    string.Join(", ", Enum.GetNames(typeof(TileAnimationType)).ToList().Remove("None")));
            }

            if (Type == TileAnimationType.Hover) {
                HoverMax = config.GetDouble("max", HoverMax);
                HoverMin = config.GetDouble("min", HoverMin);
                HoverStep = config.GetDouble("step", HoverStep);
            } else if (Type == TileAnimationType.Shake) {

            } else if (Type == TileAnimationType.Rotate) {
                XRotate = config.GetDouble("x", XRotate);
                YRotate = config.GetDouble("y", YRotate);
                ZRotate = config.GetDouble("z", ZRotate);
            } else if (Type == TileAnimationType.GrowShrink) {

            }
        }
    }
}
