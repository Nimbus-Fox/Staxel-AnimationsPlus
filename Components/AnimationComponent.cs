﻿using System.Collections.Generic;
using NimbusFox.AnimationsPlus.Classes;
using Plukit.Base;

namespace NimbusFox.AnimationsPlus.Components {
    public class AnimationComponent {
        public Vector3D TileOffset { get; }
        public Vector3D CollisionBox { get; }
        public IReadOnlyList<TileAnimation> Animations { get; }
        public AnimationComponent(Blob config) {
            TileOffset = config.FetchBlob("tileOffset").GetVector3D();

            CollisionBox = config.Contains("collisionBox") ? config.FetchBlob("collisionBox").GetVector3D() : new Vector3D(0.5, 0.5, 0.5);

            var animations = new List<TileAnimation>();

            foreach (var blob in config.FetchList("animations")) {
                animations.Add(new TileAnimation(blob.Blob()));
            }

            Animations = animations;
        }
    }
}
