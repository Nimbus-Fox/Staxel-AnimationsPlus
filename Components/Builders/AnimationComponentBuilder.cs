﻿using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.AnimationsPlus.Components.Builders {
    public class AnimationComponentBuilder : IComponentBuilder {
        public string Kind() {
            return "animationsPlus";
        }

        public object Instance(Blob config) {
            return new AnimationComponent(config);
        }
    }
}
