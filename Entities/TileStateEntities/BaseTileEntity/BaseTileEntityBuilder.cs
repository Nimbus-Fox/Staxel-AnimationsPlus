﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.AnimationsPlus.Entities.TileStateEntities.BaseTileEntity {
    public class BaseTileEntityBuilder : IEntityLogicBuilder2, IEntityPainterBuilder {
        public string Kind => KindCode;
        public static string KindCode => "nimbusfox.animationsplus.entity.baseTileEntity";

        public EntityLogic Instance(Entity entity, bool server) {
            return new BaseTileEntityLogic(entity);
        }

        public EntityPainter Instance() {
            return new BaseTileEntityPainter();
        }

        public void Load() {

        }

        public bool IsTileStateEntityKind() {
            return true;
        }

        public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, Tile tile, string kind = "nimbusfox.animationsplus.entity.baseTileEntity") {
            var entity = new Entity(universe.AllocateNewEntityId(), false, kind, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", kind);
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetString("tile", tile.Configuration.Code);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}
