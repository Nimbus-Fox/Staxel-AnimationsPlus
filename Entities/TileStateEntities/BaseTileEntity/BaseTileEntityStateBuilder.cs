﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.AnimationsPlus.Entities.TileStateEntities.BaseTileEntity {
    public class BaseTileEntityStateBuilder : ITileStateBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return "nimbusfox.animationsplus.tilestate.animationTile";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return BaseTileEntityBuilder.Spawn(location, universe, tile);
        }
    }
}
