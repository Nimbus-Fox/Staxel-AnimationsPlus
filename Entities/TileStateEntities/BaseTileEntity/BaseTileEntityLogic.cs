﻿using NimbusFox.AnimationsPlus.Components;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.AnimationsPlus.Entities.TileStateEntities.BaseTileEntity {
    public class BaseTileEntityLogic : TileStateEntityLogic {
        
        public TileConfiguration Configuration { get; private set; }
        public AnimationComponent Component { get; private set; }
        public double Rotation { get; private set; }

        protected bool NeedStore { get; private set; }

        public BaseTileEntityLogic(Entity entity) : base(entity) {
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                if (tile.Variant() != Rotation) {
                    Rotation = tile.Configuration.GetRotationInRadians(tile.Variant());
                    NeedsStore();
                }

            }
        }
        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }
        
        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                if (tile.Configuration.Code != Configuration.Code) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);
                }
            }
        }

        public override void Store() {
            if (NeedStore) {
                Entity.Blob.SetDouble("rotation", Rotation);
                Entity.Blob.SetString("tile", Configuration.Code);
                NeedStore = false;
            }
        }

        public override void Restore() {
            if (Entity.Blob.Contains("rotation")) {
                Rotation = Entity.Blob.GetDouble("rotation");
            }

            if (Entity.Blob.Contains("tile")) {
                Configuration = GameContext.TileDatabase.GetTileConfiguration(Entity.Blob.GetString("tile"));
                Component = Configuration.Components.Get<AnimationComponent>();
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            Entity.Physics.MakePhysicsless();

            Configuration = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));

            Component = Configuration.Components.Get<AnimationComponent>();

            Entity.Physics.ForcedPosition(arguments.FetchBlob("location").GetVector3I().ToTileCenterVector3D() + Component.TileOffset);

            Location = arguments.FetchBlob("location").GetVector3I();

            NeedsStore();
        }
        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
        }

        public override bool CanChangeActiveItem() {
            return false;
        }

        public override bool IsLingering() {
            return false;
        }

        public override void KeepAlive() { }
        public override void BeingLookedAt(Entity entity) { }
        public override bool IsBeingLookedAt() {
            return false;
        }

        public override Heading Heading() {
            return new Heading();
        }

        public override bool IsPersistent() {
            return false;
        }

        public override void StorePersistenceData(Blob data) { }
        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) { }
        public override bool IsCollidable() {
            return true;
        }

        protected void NeedsStore() {
            NeedStore = true;
        }
    }
}
