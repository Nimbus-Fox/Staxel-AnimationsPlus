﻿using System;
using Microsoft.Xna.Framework;
using NimbusFox.AnimationsPlus.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Effects;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.AnimationsPlus.Entities.TileStateEntities.BaseTileEntity {
    public class BaseTileEntityPainter : EntityPainter {
        protected EffectRenderer EffectRenderer = Allocator.EffectRenderer.Allocate();
        protected override void Dispose(bool disposing) { }

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade,
            int updateSteps) {
            EffectRenderer.RenderUpdate(timestep, entity.Effects, entity, (EntityPainter)this, facade, entity.Physics.Position);
        }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade) {
            if (entity.Logic is BaseTileEntityLogic logic) {
                entity.Physics.CollisionShape = Shape.MakeBox(Vector3D.Zero, logic.Component.CollisionBox);
                entity.Physics.BoundingShape = entity.Physics.CollisionShape;
            }
        }
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity, AvatarController avatarController,
            Timestep renderTimestep) { }

        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            EffectRenderer.Render(entity, this, renderTimestep, graphics, ref matrix, renderOrigin, renderMode);
            if (entity.Logic is BaseTileEntityLogic logic) {
                if (logic.Component == null) {
                    return;
                }

                var axis = new Vector3F((float)logic.Rotation, 0, 0);
                var scale = Vector3F.One;

                var resetAxis = true;

                foreach (var animation in logic.Component.Animations) {
                    if (animation.Type == TileAnimation.TileAnimationType.Rotate) {
                        if (resetAxis) {
                            axis = Vector3F.Zero;
                            resetAxis = false;
                        }
                        axis = RotationPaint(animation, axis, renderMode);
                    }

                    if (animation.Type == TileAnimation.TileAnimationType.GrowShrink) {
                        scale = GrowShrinkPaint(animation, scale, renderMode);
                    }
                }

                var originalMatrix = Matrix.CreateFromYawPitchRoll(axis.X, axis.Y, axis.Z).ToMatrix4F().Scale(scale);

                originalMatrix =
                    Matrix4F.Multiply(
                        originalMatrix.Translate(entity.Physics.Position.ToVector3F() - renderOrigin.ToVector3F()), matrix);

                foreach (var animation in logic.Component.Animations) {
                    if (animation.Type == TileAnimation.TileAnimationType.Hover) {
                        originalMatrix = HoverPaint(animation, originalMatrix, renderMode);
                    }

                    if (animation.Type == TileAnimation.TileAnimationType.Shake) {
                        originalMatrix = ShakePaint(animation, originalMatrix, renderMode);
                    }
                }

                logic.Configuration.Icon.Render(graphics, ref originalMatrix);
            }
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }

        private double _currentHover;
        private bool _up;
        private Matrix4F HoverPaint(TileAnimation animation, Matrix4F matrix, RenderMode renderMode) {
            if (renderMode == RenderMode.Normal) {
                if (_currentHover <= animation.HoverMin) {
                    _currentHover = animation.HoverMin;
                    _up = true;
                }

                if (_currentHover >= animation.HoverMax) {
                    _currentHover = animation.HoverMax;
                    _up = false;
                }

                _currentHover += _up ? animation.HoverStep : -animation.HoverStep;
            }

            return matrix.Translate(new Vector3D(0, _currentHover, 0).ToVector3F());
        }

        private Vector3F _currentRotation = Vector3F.Zero;
        private Vector3F RotationPaint(TileAnimation animation, Vector3F axis, RenderMode renderMode) {
            if (renderMode == RenderMode.Normal) {
                _currentRotation = new Vector3D(
                    (DateTime.UtcNow - new DateTime(0L)).TotalMilliseconds * animation.XRotate % (2 * Math.PI),
                    (DateTime.UtcNow - new DateTime(0L)).TotalMilliseconds * animation.YRotate % (2 * Math.PI),
                    (DateTime.UtcNow - new DateTime(0L)).TotalMilliseconds * animation.ZRotate % (2 * Math.PI)
                ).ToVector3F();
            }

            return _currentRotation + axis;
        }

        private Vector3F GrowShrinkPaint(TileAnimation animation, Vector3F scale, RenderMode renderMode) {
            return scale;
        }

        private Matrix4F ShakePaint(TileAnimation animation, Matrix4F matrix, RenderMode renderMode) {
            if (renderMode == RenderMode.Normal) {
                return matrix;
            }

            return matrix;
        }
    }
}
